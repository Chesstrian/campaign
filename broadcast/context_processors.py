def menu(request):
    _menu = dict(
        menu_users='',
        menu_packages='',
        menu_campaigns='',
        menu_lists='',
        menu_stats='',
        menu_setup='',
    )

    if '/users/' in request.path:
        _menu['menu_users'] = 'active'
    elif '/packages/' in request.path:
        _menu['menu_packages'] = 'active'
    elif '/campaigns/' in request.path:
        _menu['menu_campaigns'] = 'active'
    elif '/lists/' in request.path:
        _menu['menu_lists'] = 'active'
    elif '/reports/' in request.path:
        _menu['menu_stats'] = 'active'
    elif '/setup/' in request.path:
        _menu['menu_setup'] = 'active'

    return _menu
