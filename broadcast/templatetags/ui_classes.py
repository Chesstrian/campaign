from django import template

from broadcast.constants import PhoneLogStatus

register = template.Library()


@register.filter
def status_label(value):
    if value == PhoneLogStatus.LOADED:
        return 'success'
    elif value == PhoneLogStatus.SENDING:
        return 'info'
    elif value == PhoneLogStatus.SENT:
        return 'primary'
    elif value == PhoneLogStatus.FAILED:
        return 'warning'
