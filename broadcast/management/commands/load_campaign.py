from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from broadcast.constants import CampaignStatus
from broadcast.constants import PhoneLogStatus
from broadcast.constants import ServiceType
from broadcast.models import Campaign
from broadcast.models import Log
from broadcast.models import Setup
from broadcast.models import UserPackage


class Command(BaseCommand):
    help = "Load a campaign into database making it ready to run, it's based on id passed as argument"

    def add_arguments(self, parser):
        parser.add_argument('cid', type=int)

    def handle(self, *args, **options):
        cid = options['cid']
        try:
            campaign = Campaign.objects.get(pk=cid)
            sms_value = Setup.objects.get(label='sms_value').value
        except ObjectDoesNotExist:
            return 'Invalid campaign id'

        time = campaign.times + 1

        if campaign.campaign_type == ServiceType.SMS:
            message = campaign.message
        elif campaign.campaign_type == ServiceType.VOICE:
            message = campaign.voice
        else:
            return 'Service type not implemented'

        loaded = 0

        for phone_list in campaign.lists.all():
            for phone in phone_list.phone_list.all():
                if Log.objects.filter(
                        campaign=campaign,
                        phone=phone,
                        status=PhoneLogStatus.LOADED,
                        message=message
                ):
                    continue

                allowed, package = self.check_allowance(campaign.packages.all(), campaign.owner.profile, sms_value)
                if allowed:
                    log = Log(campaign=campaign, phone=phone, status=PhoneLogStatus.LOADED, time=time, message=message)
                    if isinstance(package, UserPackage):
                        log.package = package
                    else:
                        log.value = package

                    log.save()
                    loaded += 1
                else:
                    break

        if loaded > 0:
            campaign.times += 1
            campaign.status = CampaignStatus.LOADED
            campaign.save()

        return '{} phones were loaded'.format(loaded)

    @staticmethod
    def check_allowance(packages, profile, rate):
        for package in packages:
            if package.left > 0:
                package.left -= 1
                package.save()
                return True, package

        if rate <= profile.balance:
            profile.balance -= rate
            profile.save()
            return True, rate

        return False, 0
