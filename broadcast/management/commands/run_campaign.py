from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from infobip.api.model.Destination import Destination
from infobip.api.model.sms.mt.send.Message import Message
from infobip.api.model.sms.mt.send.textual.SMSAdvancedTextualRequest import SMSAdvancedTextualRequest
from infobip.clients import send_multiple_textual_sms_advanced
from infobip.util.configuration import Configuration

from broadcast.constants import PhoneLogStatus
from broadcast.constants import ServiceType
from broadcast.models import Campaign
from broadcast.models import Log
from broadcast.models import Setup


class Command(BaseCommand):
    help = "Run a campaign based on id passed as argument"

    def add_arguments(self, parser):
        parser.add_argument('cid', type=int)

    def handle(self, *args, **options):
        cid = options['cid']
        try:
            campaign = Campaign.objects.get(pk=cid)
            username = Setup.objects.get(label='infobip_username').value
            password = Setup.objects.get(label='infobip_password').value
            country_code = Setup.objects.get(label='country_code').value
        except ObjectDoesNotExist:
            return 'Invalid campaign id or missing data setup'

        if campaign.campaign_type != ServiceType.SMS:
            return 'Type not yet implemented'

        send_sms_client = send_multiple_textual_sms_advanced(Configuration(username=username, password=password))

        message = Message()
        message.text = campaign.message
        message.notify_url = "http://mensajes.123soluciones.com/delivery/"
        message.destinations = []

        for log in Log.objects.filter(campaign_id=cid, status=PhoneLogStatus.LOADED):
            destination = Destination()
            destination.message_id = log.id
            destination.to = country_code + log.phone

            message.destinations.append(destination)

            log.status = PhoneLogStatus.SENDING
            log.last_sent = now()
            log.save()

        request = SMSAdvancedTextualRequest()
        request.messages = [message]

        response = send_sms_client.execute(request)

        print(response)
