from django.core.management.base import BaseCommand
from infobip.api.model.Destination import Destination
from infobip.api.model.sms.mt.send.Message import Message
from infobip.api.model.sms.mt.send.textual.SMSAdvancedTextualRequest import SMSAdvancedTextualRequest
from infobip.clients import send_multiple_textual_sms_advanced
from infobip.util.configuration import Configuration


class Command(BaseCommand):
    help = "Send a test sms message"

    def handle(self, *args, **options):
        username = 'Home12345'
        password = 'Infobip12'

        send_sms_client = send_multiple_textual_sms_advanced(Configuration(username=username, password=password))

        destination = Destination()
        destination.message_id = 1
        destination.to = "3192381747"

        message = Message()
        message.text = "Test message sent by Chess"
        message.notify_url = "http://mensajes.123soluciones.com/delivery/"
        message.destinations = [destination]

        request = SMSAdvancedTextualRequest()
        request.messages = [message]

        response = send_sms_client.execute(request)

        print(response)
