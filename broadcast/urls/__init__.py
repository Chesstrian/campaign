from broadcast.urls.campaigns import urlpatterns as urls_campaigns
from broadcast.urls.lists import urlpatterns as urls_lists
from broadcast.urls.packages import urlpatterns as urls_packages
from broadcast.urls.users import urlpatterns as urls_users
from broadcast.urls.others import urlpatterns as urls_others

urlpatterns = urls_campaigns + urls_lists + urls_packages + urls_users + urls_others
