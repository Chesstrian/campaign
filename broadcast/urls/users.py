from django.conf.urls import url

from broadcast import views

urlpatterns = [

    url(
        r'^users/list/$',
        view=views.UserList.as_view(),
        name='user_list'
    ),
    url(
        r'^users/add/$',
        view=views.UserCreate.as_view(),
        name='user_add'
    ),
    url(
        r'^users/update/(?P<pk>\d+)/$',
        view=views.user_update,
        name='user_update'
    ),
    url(
        r'^users/delete/(?P<pk>\d+)/$',
        view=views.UserDelete.as_view(),
        name='user_delete'
    ),

]
