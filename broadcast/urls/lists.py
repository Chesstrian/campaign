from django.conf.urls import url

from broadcast import views

urlpatterns = [

    url(
        r'^lists/list/$',
        view=views.PhoneListList.as_view(),
        name='list_list'
    ),
    url(
        r'^lists/add/$',
        view=views.PhoneListCreate.as_view(),
        name='list_add'
    ),
    url(
        r'^lists/update/(?P<pk>\d+)/$',
        view=views.PhoneListUpdate.as_view(),
        name='list_update'
    ),
    url(
        r'^lists/delete/(?P<pk>\d+)/$',
        view=views.PhoneListDelete.as_view(),
        name='list_delete'
    ),

]
