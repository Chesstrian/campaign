from django.conf.urls import url

from broadcast import views

urlpatterns = [

    url(
        r'^delivery/$',
        view=views.delivery,
        name='delivery'
    ),
    url(
        r'^load_campaign/(?P<pk>\d+)/$',
        view=views.load_campaign,
        name='load_campaign'
    ),
    url(
        r'^reports/$',
        view=views.ReportList.as_view(),
        name='reports'
    ),
    url(
        r'^run_campaign/(?P<pk>\d+)/$',
        view=views.run_campaign,
        name='run_campaign'
    ),
    url(
        r'^setup/$',
        view=views.setup,
        name='setup'
    ),

]
