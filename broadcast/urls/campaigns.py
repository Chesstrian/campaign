from django.conf.urls import url

from broadcast import views

urlpatterns = [

    url(
        r'^campaigns/list/$',
        view=views.CampaignList.as_view(),
        name='campaign_list'
    ),
    url(
        r'^campaigns/add/$',
        view=views.CampaignCreate.as_view(),
        name='campaign_add'
    ),
    url(
        r'^campaigns/update/(?P<pk>\d+)/$',
        view=views.CampaignUpdate.as_view(),
        name='campaign_update'
    ),
    url(
        r'^campaigns/delete/(?P<pk>\d+)/$',
        view=views.CampaignDelete.as_view(),
        name='campaign_delete'
    ),

]
