from django.conf.urls import url

from broadcast import views

urlpatterns = [
    url(
        r'^packages/list/$',
        view=views.PackageList.as_view(),
        name='package_list'
    ),
    url(
        r'^packages/add/$',
        view=views.PackageCreate.as_view(),
        name='package_add'
    ),
    url(
        r'^packages/update/(?P<pk>\d+)/$',
        view=views.PackageUpdate.as_view(),
        name='package_update'
    ),
    url(
        r'^packages/delete/(?P<pk>\d+)/$',
        view=views.PackageDelete.as_view(),
        name='package_delete'
    ),
]
