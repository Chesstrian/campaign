from django import forms
from django.forms.models import BaseInlineFormSet

from broadcast.models import Profile


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('balance',)


class UserPackageFormset(BaseInlineFormSet):
    def clean(self):
        super(UserPackageFormset, self).clean()

        for form in self.forms:
            if not 'package' in form.cleaned_data:
                form.cleaned_data['DELETE'] = True
                form.instance.DELETE = True

                continue

            form.cleaned_data['left'] = form.cleaned_data['package'].bucket
            form.instance.left = form.cleaned_data['package'].bucket
