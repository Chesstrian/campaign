from broadcast.constants import CampaignStatus
from broadcast.constants import PhoneLogStatus
from broadcast.constants import ServiceType
from broadcast.constants import ValueType

CampaignStatusName = (
    (CampaignStatus.LOADED, u'LOADED'),
    (CampaignStatus.RUNNING, u'RUNNING'),
    (CampaignStatus.STOPPED, u'STOPPED'),
    (CampaignStatus.FINISHED, u'FINISHED')
)

PhoneLogStatusName = (
    (PhoneLogStatus.LOADED, u'LOADED'),
    (PhoneLogStatus.SENDING, u'SENDING'),
    (PhoneLogStatus.SENT, u'SENT'),
    (PhoneLogStatus.FAILED, u'FAILED')
)

ServiceTypeName = (
    (ServiceType.SMS, u'SMS'),
    (ServiceType.VOICE, u'VOICE')
)

ValueTypeName = (
    (ValueType.INTEGER, u'INTEGER'),
    (ValueType.FLOAT, u'FLOAT'),
    (ValueType.STRING, u'STRING')
)
