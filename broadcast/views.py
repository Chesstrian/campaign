# -*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core.management import call_command
from django.db import transaction
from django.forms.models import inlineformset_factory
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.urls.base import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic.base import View
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from infobip.api.model.sms.mt.reports.SMSReportResponse import SMSReportResponse

from broadcast.constants import PhoneLogStatus
from broadcast.forms import ProfileForm
from broadcast.forms import UserPackageFormset
from broadcast.models import Campaign
from broadcast.models import Log
from broadcast.models import Package
from broadcast.models import Phone
from broadcast.models import PhoneList
from broadcast.models import Setup
from broadcast.models import UserPackage


class UserIsSuperUserMixin(UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_superuser


class CampaignList(UserIsSuperUserMixin, ListView):
    model = Campaign
    context_object_name = 'campaigns'


class CampaignCreate(SuccessMessageMixin, UserIsSuperUserMixin, CreateView):
    model = Campaign
    fields = '__all__'
    success_message = u'Campaña %(name)s creada satisfactoriamente'
    success_url = reverse_lazy('campaign_list')


class CampaignUpdate(SuccessMessageMixin, UserIsSuperUserMixin, UpdateView):
    model = Campaign
    fields = '__all__'
    success_message = u'Campaña %(name)s actualizada satisfactoriamente'
    success_url = reverse_lazy('campaign_list')


class CampaignDelete(SuccessMessageMixin, UserIsSuperUserMixin, DeleteView):
    model = Campaign
    success_message = u'Campaña %(name)s borrada satisfactoriamente'
    success_url = reverse_lazy('campaign_list')


class PackageList(UserIsSuperUserMixin, ListView):
    model = Package
    context_object_name = 'packages'


class PackageCreate(SuccessMessageMixin, UserIsSuperUserMixin, CreateView):
    model = Package
    fields = ('name', 'package_type', 'description', 'bucket', 'price', 'is_active')
    success_message = 'Paquete %(name)s creado satisfactoriamente'
    success_url = reverse_lazy('package_list')


class PackageUpdate(SuccessMessageMixin, UserIsSuperUserMixin, UpdateView):
    model = Package
    fields = ('name', 'package_type', 'description', 'bucket', 'price', 'is_active')
    success_message = 'Paquete %(name)s actualizado satisfactoriamente'
    success_url = reverse_lazy('package_list')


class PackageDelete(SuccessMessageMixin, UserIsSuperUserMixin, DeleteView):
    model = Package
    success_message = 'Paquete %(name)s borrado satisfactoriamente'
    success_url = reverse_lazy('package_list')


class PhoneListList(UserIsSuperUserMixin, ListView):
    model = PhoneList
    context_object_name = 'phone_lists'


class PhoneListCreate(SuccessMessageMixin, UserIsSuperUserMixin, CreateView):
    model = PhoneList
    fields = '__all__'
    success_message = 'Lista %(name)s creada satisfactoriamente'
    success_url = reverse_lazy('list_list')


class PhoneListUpdate(SuccessMessageMixin, UserIsSuperUserMixin, UpdateView):
    model = PhoneList
    fields = '__all__'
    success_message = 'Lista %(name)s actualizada satisfactoriamente'
    success_url = reverse_lazy('list_list')

    def post(self, request, **kwargs):
        phones_file = request.FILES.get('phones')
        phones = phones_file.get_sheet().column[0] if phones_file else []

        for phone in phones:
            Phone(phone_list_id=kwargs['pk'], number=phone).save()

        return super(PhoneListUpdate, self).post(request, **kwargs)


class PhoneListDelete(SuccessMessageMixin, UserIsSuperUserMixin, DeleteView):
    model = PhoneList
    success_message = 'Lista %(name)s borrada satisfactoriamente'
    success_url = reverse_lazy('list_list')


class ReportList(UserIsSuperUserMixin, ListView):
    model = Log
    context_object_name = 'reports'

    def get_context_data(self, **kwargs):
        context = super(ReportList, self).get_context_data(**kwargs)
        context['campaigns'] = Campaign.objects.exclude(status__isnull=True)

        if 'campaign' in self.request.GET:
            context['selected_campaign'] = self.request.GET['campaign']

        return context

    def get_queryset(self):
        queryset = Log.objects.none()

        if 'campaign' in self.request.GET:
            campaign = self.request.GET['campaign']
            if campaign != '':
                queryset = Log.objects.filter(campaign_id=campaign)

        return queryset


class UserList(UserIsSuperUserMixin, ListView):
    model = User
    context_object_name = 'users'


class UserCreate(SuccessMessageMixin, UserIsSuperUserMixin, CreateView):
    model = User
    form_class = UserCreationForm
    success_message = 'Usuario %(username)s creado satisfactoriamente'
    success_url = reverse_lazy('user_list')
    template_name = 'auth/user_add_form.html'


@transaction.atomic
@user_passes_test(lambda u: u.is_superuser)
def user_update(request, pk):
    user = User.objects.get(pk=pk)
    # noinspection PyPep8Naming
    UserPackageFormSet = inlineformset_factory(
        User,
        UserPackage,
        formset=UserPackageFormset,
        fields=('package', 'left',),
        extra=1
    )

    if request.method == 'POST':
        user_form = UserChangeForm(request.POST, instance=user)
        profile_form = ProfileForm(request.POST, instance=user.profile)
        formset = UserPackageFormSet(request.POST, instance=user)

        if user_form.is_valid() and profile_form.is_valid() and formset.is_valid():
            user_form.save()
            profile_form.save()
            formset.save()

            messages.success(request, 'Usuario {} actualizado satisfactoriamente'.format(user.username))
            return redirect('user_list')
    else:
        user_form = UserChangeForm(instance=user)
        profile_form = ProfileForm(instance=user.profile)
        formset = UserPackageFormSet(instance=user)

    return render(request, 'auth/user_edit_form.html', {
        'form': user_form,
        'profile_form': profile_form,
        'formset': formset
    })


class UserDelete(SuccessMessageMixin, UserIsSuperUserMixin, DeleteView):
    model = User
    success_message = 'Usuario %(username)s borrado satisfactoriamente'
    success_url = reverse_lazy('user_list')


@csrf_exempt
@require_POST
def delivery(request):
    reports = SMSReportResponse.from_JSON(request.body)

    for result in reports["results"]:
        phone_log = Log.objects.get(pk=result["messageId"])
        if result['status']["name"] == "DELIVERED_TO_HANDSET":
            phone_log.status = PhoneLogStatus.SENT
        else:
            phone_log.status = PhoneLogStatus.FAILED
            if phone_log.package:
                phone_log.package.left += 1
            else:
                phone_log.campaign.owner.profile.balance += phone_log.value

        phone_log.save()

    return JsonResponse(dict(processed=True))


@login_required
def load_campaign(request, pk):
    # TODO: Check campaign owns to logged user, or user is admin.

    result = call_command('load_campaign', pk)

    messages.success(request, result)
    return redirect('campaign_update', pk=pk)


@login_required
def run_campaign(request, pk):
    # TODO: Check campaign owns to logged user, or user is admin.

    result = call_command('run_campaign', pk)

    messages.success(request, result)
    return redirect('campaign_update', pk=pk)


@transaction.atomic
@user_passes_test(lambda u: u.is_superuser)
def setup(request):
    fields = Setup.objects.all()

    if request.method == 'POST':
        for field in fields:
            field.value = request.POST.get(field.label)
            field.save()

        messages.success(request, 'Cambios guardados exitosamente')

    return render(request, 'broadcast/setup_form.html', {
        'fields': fields
    })
