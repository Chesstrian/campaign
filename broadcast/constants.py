class CampaignStatus:
    LOADED = 0
    RUNNING = 1
    STOPPED = 2
    FINISHED = 3


class PhoneLogStatus:
    LOADED = 0
    SENDING = 1
    SENT = 2
    FAILED = 3


class ServiceType:
    SMS = 0
    VOICE = 1


class ValueType:
    INTEGER = 0
    FLOAT = 1
    STRING = 2
