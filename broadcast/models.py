from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from broadcast import choices


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    balance = models.FloatField(null=True, blank=True, default=0)


# noinspection PyUnusedLocal
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


# noinspection PyUnusedLocal
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Package(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, db_index=True)
    package_type = models.PositiveIntegerField(null=False, blank=False, choices=choices.ServiceTypeName)
    description = models.TextField(null=True, blank=True)
    bucket = models.PositiveSmallIntegerField(null=False, blank=False)
    price = models.FloatField(null=False, blank=False)

    clients = models.ManyToManyField(to=User, blank=True, through='UserPackage', through_fields=('package', 'user'))

    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.package_type_name, self.name)

    @property
    def package_type_name(self):
        return choices.ServiceTypeName[self.package_type][1]


class UserPackage(models.Model):
    package = models.ForeignKey(Package, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    left = models.PositiveSmallIntegerField(null=False, blank=False)

    def __str__(self):
        return self.package.__str__()


class PhoneList(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, db_index=True)
    description = models.TextField(null=True, blank=True)

    owner = models.ForeignKey(to=User, blank=False)

    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    @property
    def list_length(self):
        return self.phone_list.count()


class Phone(models.Model):
    number = models.CharField(max_length=20, null=False, blank=False)
    phone_list = models.ForeignKey(to=PhoneList, related_name='phone_list')

    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Campaign(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, db_index=True)
    campaign_type = models.PositiveIntegerField(null=False, blank=False, choices=choices.ServiceTypeName)
    description = models.TextField(null=True, blank=True)

    message = models.CharField(max_length=160, null=True, blank=True)
    voice = models.CharField(max_length=160, null=True, blank=True)

    owner = models.ForeignKey(to=User, blank=False)
    packages = models.ManyToManyField(to=UserPackage, blank=True)
    lists = models.ManyToManyField(to=PhoneList, blank=True)

    status = models.PositiveIntegerField(null=True, blank=True, choices=choices.CampaignStatusName)
    times = models.PositiveSmallIntegerField(null=True, blank=True, default=0)

    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    @property
    def campaign_type_name(self):
        return choices.ServiceTypeName[self.campaign_type][1]

    @property
    def status_name(self):
        return choices.CampaignStatusName[self.status][1]


class Log(models.Model):
    campaign = models.ForeignKey(to=Campaign)
    phone = models.ForeignKey(to=Phone)

    message = models.CharField(max_length=160, null=True, blank=True)  # It can change and rerun the campaign

    package = models.ForeignKey(to=UserPackage, blank=True, null=True)
    value = models.PositiveSmallIntegerField(default=0)  # In case balance is used

    status = models.PositiveIntegerField(null=False, blank=False, choices=choices.PhoneLogStatusName)
    time = models.PositiveSmallIntegerField(null=False, blank=False)
    last_sent = models.DateTimeField(null=True, blank=True)

    @property
    def status_name(self):
        return choices.PhoneLogStatusName[self.status][1]


class Setup(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, db_index=True)
    label = models.CharField(max_length=100, null=False, blank=False, db_index=True)
    value = models.CharField(max_length=100, null=True, blank=True)

    value_type = models.PositiveIntegerField(null=True, blank=True, choices=choices.ValueTypeName)

    def __str__(self):
        return self.name.encode('utf-8')

    @property
    def value_type_name(self):
        return choices.ValueTypeName[self.value_type][1]
